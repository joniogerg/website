var logoTrigger = 70;
var logoNav = 70, logoScale = 50;
const navGifIc = "./assets/images/anim-logo.gif";
const navIc = "./assets/images/logo.png";

$(document).ready(function(){
    
    rescale();
    adjustLogo();

    removeSplashScreen();
    
    $(window).scroll(function(ev){
        if($("#splash").css("top") == window.innerHeight + "px"){
            adjustLogo();
        }
    });
    
    $(window).resize(function(ev){
        if($("#splash").css("top") == window.innerHeight + "px"){
            adjustLogo();
            rescale();
        }
    });
    
});

function removeSplashScreen(){

    $("#logo-nav-src").attr("src", navGifIc);

    var elLogo = $("#logo-nav");
    elLogo.css("height", window.innerHeight / 2);
    elLogo.css("width", window.innerHeight / 2);
    elLogo.css("top", window.innerHeight / 4);
    elLogo.css("left", (window.innerWidth - window.innerHeight / 2) / 2);
    setTimeout(function (){
        elLogo.find("svg .cls-2").css("stroke-width", "90px");
    }, 1900);
    setTimeout(function (){
        $("#logo-nav-src").attr("src", navIc);
        elLogo.css("left", window.innerWidth / 2 - logoScale / 2 - 5);
        elLogo.css("top", logoTrigger);
        elLogo.css("height", logoScale);
        elLogo.css("width", logoScale);
        if(window.scrollY > logoTrigger){
            elLogo.css("left", window.innerWidth / 2 - logoNav / 2 - 5);
            elLogo.css("top", 0);
            elLogo.css("height", logoNav);
            elLogo.css("width", logoNav);
        }
        elLogo.css("opacity", "1");
        setTimeout(function (){
            elLogo.css("transition", "none");
            $("#splash").css("top", "100%");
            $("#nav").removeClass("hidden");
            $("html, body").css("overflow", "auto");
        }, 700);
        
    }, 2000);
}

function adjustLogo(){
    var elLogo = $("#logo-nav");
    var scale = ((logoNav - logoScale) / logoTrigger) * window.scrollY
    if(scale + logoScale <= logoNav)
        scale = (logoNav - logoScale);
    elLogo.css("height", logoScale + scale);
    elLogo.css("width", logoScale + scale);
    if(window.scrollY > logoTrigger)
        elLogo.css("top", 0);
    else
        elLogo.css("top", logoTrigger - window.scrollY);
    elLogo.css("left", window.innerWidth / 2 - elLogo.outerWidth() / 2 - 5);

    $(".logo-spacer").css("width", elLogo.innerWidth + 20);

    if(elLogo.innerHeight() == logoNav){
        $("#nav").addClass("scaled");
    }else{
        $("#nav").removeClass("scaled");
    }
}

function rescale(){
    logoTrigger = ($($("#intro .wrapper .content").children().get(0)).offset().top -$("#logo-nav").innerHeight()) / 2;
    logoScale = logoTrigger;
    if(logoScale < logoNav) logoScale = logoNav;
}